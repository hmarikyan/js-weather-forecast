##Summmary

A really tiny weather forecast app.

Cities list is mocked.
API gives 5 days, per 3 hours information about weather.
Used geolocation api to pull client's coordinates and select city in the list. 

Written using javascript/html/css.

##Note
Webpack is not configured to copy html/css files from 'src' to 'build' folder so:
Please don't remove './build' folder, allowed to remove only .js.map and .js files. ))


##Installation
1. Install node.js (skip, if you already have it)
2. Clone the project
3. Switch to project's directory and Run 'npm install'
4. Run 'npm start',
5. Install http-server - "run http-server -g"
6. Run "http-server ./build"
7. Open "http://127.0.0.1:8080/" in browser