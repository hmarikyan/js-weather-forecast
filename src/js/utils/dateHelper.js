/**
 * Map to array of {value, text} objects
 * @param list
 * @returns {*|Array}
 */
export const parseDates = ({list}) => {
    return list.map((item, index) => {
        const itemDate = new Date(item.dt * 1000);
        return {
            value: index,
            text: `${itemDate.toLocaleDateString()} ${itemDate.getHours()}:00`
        }
    });
};