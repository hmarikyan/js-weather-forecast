export const cities = [
    {value: 'London', text: 'London',  selected: true},
    {value: 'Berlin', text: 'Berlin'},
    {value: 'Yerevan', text: 'Yerevan'},
    {value: 'Moscow', text: 'Moscow'},
    {value: 'Dublin', text: 'Dublin'},
    {value: 'Sydney', text: 'Sydney'}
];