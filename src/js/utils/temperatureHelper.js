const convertKelvinToCelsius = tempKelvin => tempKelvin - 273;

const formatCelsius = tempKelvin => `${convertKelvinToCelsius(tempKelvin).toFixed()} C`;

export {
    convertKelvinToCelsius,
    formatCelsius
}