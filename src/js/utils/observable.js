class Observable {
    handlers = [];

    subscribe(event, listener) {
        if(!this.handlers[event]) {
            this.handlers[event] = [];
        }

        // push the listener
        this.handlers[event].push(listener);
    }

    publish(event, data) {
        // no-op if the topic doesn't exist, or there are no listeners
        if(!this.handlers[event] || this.handlers[event].length < 1) return;

        // send the event to all listeners
        this.handlers[event].forEach(listener => listener(data || {}));
    }

    //TODO: to be implemented
    //unsubscribe
}

export default Observable;