const API_KEY = 'bf51b6989df672e8923ba4d2a640dc7c';
const END_POINT = 'http://api.openweathermap.org/data/2.5';

class Weather{
    //make request to given url and get response
    async request (url) {
        try {
            const response = await fetch(url);
            const data = await response.json();

            return data;
        }catch(e){
            console.log(e);
            Promise.reject(e);
        }
    };

    //get weather info by geo coordinates latitude/longitude
    getByCoords(lat, lon) {
        return this.request(`${END_POINT}/weather?lat=${lat}&lon=${lon}&appid=${API_KEY}`);
    };

    //get info by city
    getByCity(cityName){
        return this.request(`${END_POINT}/forecast?q=${cityName}&appid=${API_KEY}`);
    }
}

export default new Weather();