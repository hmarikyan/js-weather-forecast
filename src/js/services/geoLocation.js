class GeoLocation{
    //used promise syntax
    get(){
        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(resolve, reject);
        })
    }
}

export default new GeoLocation();