import {cities} from './utils/mockData';
import {parseDates} from './utils/dateHelper';

import Dropdown from './components/dropdown';
import SelectableList from './components/selectableList';
import Forecast from './components/forecast';

import weather from './services/weather';
import geoLocation from './services/geoLocation';

const bootstrap = () => {
    try {
        //weather data by hours
        let forecastData = {};

        //result block
        const forecastBlock = new Forecast(document.getElementById('forecast'));

        //date/hours list
        const dateList = new SelectableList(document.getElementById('days'));
        dateList.subscribe('select', ({value: index}) => {
            //get data by selected index and render
            const dataToRender = Object.assign({}, forecastData.list[index], {city: forecastData.city});
            forecastBlock.render(dataToRender);
        });

        const citiesDropdown = new Dropdown(document.getElementById('cities'));
        citiesDropdown.render(cities);

        citiesDropdown.subscribe('change', async ({e, value:cityName}) => {
            //request forecast for selected city
            forecastData = await weather.getByCity(cityName);

            const datesList = parseDates(forecastData);
            dateList.render(datesList);

            //empty information panel
            forecastBlock.render("");
        });

        //get current coords, get current city, get weather for current city
        geoLocation.get()
            .then(pos => weather.getByCoords(+pos.coords.latitude, +pos.coords.longitude))
            .then(weatherData => citiesDropdown.selectByValue(weatherData.name))
            .catch(console.log);

    } catch (e) {
        console.log(e);
    }
};

document.addEventListener("DOMContentLoaded", bootstrap);