import Observable from '../utils/observable';

/**
 * base class
 */
class Selectable extends Observable{

    /**
     * example data = [
     *  {value: 1, text: '111', selected: false},
     *  {value: 2, text: '222', selected: true},
     * ]
     */
    data = [];


    /**
     * root element instance in dom
     * @type {null}
     */
    element = null;

    /**
     * @param data:
     */
    constructor(wrapper, data = []){
        super();
        this.data = data;
        this.wrapper = wrapper;

        this.render = this.render.bind(this);

        if(this.registerEventListeners ){
            this.registerEventListeners = this.registerEventListeners.bind(this);
        }
    }

    /**
     * no-op
     * @returns {null}
     */
    buildHtml(){
        return null;
    }

    /**
     * @param data
     */
    render(data){
        this.data = data;
        const block = this.buildHtml();

        this.wrapper.innerHTML = '';
        this.wrapper.appendChild(block);
        this.element = block;

        this.registerEventListeners && this.registerEventListeners();
    }
}


export default Selectable;