import Selectable from './selectable';

class Dropdown extends Selectable{

    /**
     * @param wrapper
     * @param data
     */
    constructor(wrapper, data = []){
        super(wrapper, data);

        this.buildHtml = this.buildHtml.bind(this);
    }

    /**
     * select option externally
     * @param value
     */
    selectByValue(value){
        this.element.value = value;
        this.publish('change', {value})
    }

    /**
     * create html markup to render in given block
     * @returns {Element}
     */
    buildHtml () {
        const selectList = document.createElement("select");

        const optionsHtml = this.data.forEach(opt => {
            const option = document.createElement("option");
            option.value = opt.value;
            option.text = opt.text;
            option.selected = opt.selected;
            selectList.appendChild(option);
        });

        return selectList;
    }

    /**
     * extend parent's function
     * @param data
     */
    render(data){
       super.render(data);

       this.registerEventListeners();
    }

    //pusblish actions whenever event fires
    registerEventListeners(){
        this.element.addEventListener('change', (e) => this.publish('change', {
            e,
            value: e.target.value
        }));
        this.element.addEventListener('click', (e) => this.publish('click', {e}));
    }
}

export default Dropdown;