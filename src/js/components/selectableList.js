import Selectable from './selectable';

/**
 * unordered list with select- feature
 */
class SelectableList extends Selectable{

    constructor(wrapper, data = []){
        super(wrapper, data);
    }

    buildHtml () {
        const ulHtml = document.createElement("ul");

        this.data.forEach(opt => {
            const li = document.createElement("li");
            li.innerText = opt.text;
            li.setAttribute('data-value', opt.value);
            li.className = opt.selected ? "selected" : '';

            //add listener on item click
            //TODO: could be added to main element, not to every single item
            li.addEventListener('click', (e) => {
                const eventPayload = {
                    value: e.target.getAttribute('data-value'),
                    e
                };

                this.selectItem(e, eventPayload);
                this.publish('select', eventPayload);
            });

            ulHtml.appendChild(li);
        });

        return ulHtml;
    }

    /**
     * Select row/item
     * @param e
     * @param data
     */
    selectItem(e, data){
        var items = Array.from(this.element.children);

        items.forEach(item => {
            if(item.getAttribute('data-value') == data.value){
                item.className = "selected";
            }else{
                item.className = "";
            }
        });
    }
}

export default SelectableList;