import {formatCelsius} from '../utils/temperatureHelper';

class Forecast{
    data = [];
    element = null;
    wrapper = null;

    /**
     * @param wrapper
     */
    constructor(wrapper){
        this.wrapper = wrapper;

        this.render = this.render.bind(this);
        this.buildHtml = this.buildHtml.bind(this);
    }

    /**
     * return icons html markup
     * @param weather
     * @returns {*}
     */
    buildIconsHtml (weather) {
        return weather.reduce((acc, item) => {
            return acc +`<div class="icon-wrapper">
                <img title="${item.description}" src="http://openweathermap.org/img/w/${item.icon}.png"/>
            </div>`;
        }, '');
    }

    /**
     * create html based on data object/array
     * @returns {string}
     */
    buildHtml () {
        const {clouds, city, main, wind, weather} = this.data;

        const iconsHTML = this.buildIconsHtml(weather);

        const div = `
            <div class="forecast">
                <h3>INFORMATION</h3>
            
                <div class="weather-icon fragment">
                       ${iconsHTML}
                </div>
            
                <div class="location fragment">
                     <h4 class="title">Location</h4>
                     <section>
                         <div class="country">Country: ${city.country}</div>
                         <div class="name">City: ${city.name}</div>
                         <div class="population">Population: ${city.population} people</div>
                     </section>
                </div>
            
                <div class="clouds fragment">
                    <h4 class="title">Clouds</h4>
                    <section>
                        Clouds: ${clouds.all} %
                    </section>
                </div>
                 
                 <div class="main fragment">
                     <h4 class="title">Main</h4>
                     <section>
                         <div class="pressure">Pressure sea: ${main.pressure} hPa</div>
                         <div class="humidity">Humidity: ${main.humidity} %</div>
                         <div class="temp">Temp: ${formatCelsius(main.temp)}</div>
                     </section>
                 </div>
               
                 <div class="wind fragment">
                     <h4 class="title">Wind</h4>
                     <section>
                         <div class="deg">Direction: ${wind.deg} degree</div>
                         <div class="speed">Speed: ${wind.speed} meter/sec</div>
                     </section>
                 </div>
            </div>
        `;

        return div;
    }

    /**
     * renderer
     */
    render(data = null){
        if(!data){
            return this.wrapper.innerHTML = '';
        }
        this.data = data;
        const block = this.buildHtml();
        this.wrapper.innerHTML = block;
        this.element = block;
    }
}

export default Forecast;